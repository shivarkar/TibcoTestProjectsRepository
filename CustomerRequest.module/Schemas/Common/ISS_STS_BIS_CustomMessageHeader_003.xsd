<?xml version="1.0" encoding="UTF-8"?>

<!--
Version history
003     11-07-2012     F. Gaus     Version related to Naming Standards 1.4.
 -->
<xsd:schema xmlns="http://www.ing.com/xsd/CustomMessageHeader_003"
	 xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	 targetNamespace="http://www.ing.com/xsd/CustomMessageHeader_003"
	 elementFormDefault="qualified"
	 attributeFormDefault="unqualified">
	<xsd:element name="CustomMessageHeader" type="CustomMessageHeaderType"/>
	<xsd:complexType name="CustomMessageHeaderType">
		<xsd:sequence>
			<xsd:element name="From" type="FromType" minOccurs="0"/>
			<xsd:element name="To" type="ToType" minOccurs="0"/>
			<xsd:element name="CollaborationInfo" type="CollaborationInfoType" minOccurs="0"/>
			<xsd:element name="MessageInfo" type="MessageInfoType" minOccurs="0"/>
			<xsd:element name="TraceHeaderList" type="TraceHeaderListType" minOccurs="0"/>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name="FromType">
		<xsd:annotation>
			<xsd:documentation>
        		Sending party of the message.
        	</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="Id" type="xsd:string">
				<xsd:annotation>
					<xsd:documentation>
						The identification of the initiator of this
						interaction. It identifies the specific instance
						of the requestor.
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name="ToType">
		<xsd:annotation>
			<xsd:documentation>
        		Party that receives the message.
        	</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="Id" type="xsd:string" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>
						The identification of the recipient. It
						identifies a logical system, a specific instance
						is usually not known by the sender.
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name="CollaborationInfoType">
		<xsd:annotation>
			<xsd:documentation>
				These elements facilitate collaboration between from and
				to parties.
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="CPAId" type="xsd:string" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>
						Collaboration Protocol Agreement Id. An id that
						identifies the agreement between the parties for
						the exchange of messages. This id is unique for
						the business value chain and is determined by
						the business. It can be set by the initiator of
						the service chain. Others involved in the chain
						should propagate this id by copying it.
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="Conversation" type="ConversationType" minOccurs="0"/>
			<xsd:element name="Paradigm">
				<xsd:annotation>
					<xsd:documentation>
						Enumerated value of paradigm. This is only an
						identification of the specific message itself
						and not a reference to a Message Exchange
						Pattern.

						Oneway: Service consumer sends message to
						service provider.

						Request: Service consumer sends message to
						service provider.

						Reply: Service Provider sends correlated message
						to service consumer.

						Solicit: Service provider initiates a request to
						service consumer.

						Response: Service consumer sends correlated
						message to service provider.

						Notification: Service provider sends message to
						service consumer.
					</xsd:documentation>
				</xsd:annotation>
				<xsd:simpleType>
					<xsd:restriction base="xsd:string">
						<xsd:enumeration value="OneWay"/>
						<xsd:enumeration value="Request"/>
						<xsd:enumeration value="Reply"/>
						<xsd:enumeration value="Solicit"/>
						<xsd:enumeration value="Response"/>
						<xsd:enumeration value="Notification"/>
					</xsd:restriction>
				</xsd:simpleType>
			</xsd:element>
			<xsd:choice>
				<xsd:element name="SOA" type="SOAType">
					<xsd:annotation>
						<xsd:documentation>
							The most common option. In the case of a SOA
							service.
						</xsd:documentation>
					</xsd:annotation>
				</xsd:element>
				<xsd:element name="EAI" type="EAIType">
					<xsd:annotation>
						<xsd:documentation>
							For an EAI pattern in case a SOA service
							cannot be defined.
						</xsd:documentation>
					</xsd:annotation>
				</xsd:element>
			</xsd:choice>
			<xsd:element name="Addressing" type="AddressingType">
				<xsd:annotation>
					<xsd:documentation/>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="PropertyList" type="PropertyListType" minOccurs="0"/>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name="PropertyListType">
		<xsd:annotation>
			<xsd:documentation>
        		The property list is an extension point for
        		CollaborationInfo, which allows the definition of
        		specific collaboration patterns without changing the
        		interface. The actual properties that can be used are
        		listed in the Naming Standards and are governed by SOA
        		Governance. It is explicitly not allowed to use
        		properties other than the published ones.
        	</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="Property" type="PropertyType" maxOccurs="unbounded"/>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name="PropertyType">
		<xsd:annotation>
			<xsd:documentation>
        		Name Value Pair describing a property. Some properties
        		do not require an explicit Value.
        	</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="Name" type="xsd:string"/>
			<xsd:element name="Value" type="xsd:string" minOccurs="0"/>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name="ConversationType">
		<xsd:annotation>
			<xsd:documentation>
        		Structure to keep track of the conversation and
        		subconversations.
        	</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="ConversationId" type="xsd:string">
				<xsd:annotation>
					<xsd:documentation>
						Element identifying a set of related messages
						that makes up a conversation within the context
						of a business chain. It is unique within the
						context of the CPAId. If the ConversationId is
						present the CPAId must be present as well.
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="SubConversationIdList" type="SubConversationIdListType" minOccurs="0"/>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name="SubConversationIdListType">
		<xsd:annotation>
			<xsd:documentation>
        		Push and pull list of SubConversationIds. New
        		subconversations that are initiated are appended at the
        		bottom.
        	</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="SubConversationId" type="xsd:string" maxOccurs="5"/>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name="MessageInfoType">
		<xsd:annotation>
			<xsd:documentation>
        		The MessageInfo element contains information about the
        		message. Its purpose is to have this information in the
        		header to make the exchange of messages transport
        		independent.
        	</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="MessageId" type="xsd:string">
				<xsd:annotation>
					<xsd:documentation>
						Unique identifier for this message within the
						scope of the ConversationId.
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="Timestamp" type="xsd:dateTime">
				<xsd:annotation>
					<xsd:documentation>
						Date and time the message was sent, including
						the time zone.
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="TTL" type="xsd:dateTime" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>
						Time To Live. This element indicates the time a
						message is valid. The field contains the
						absolute date and time (including the time zone)
						after which the message is not valid any more.
						If a recipient receives a message with a TTL
						that is expired it should not process the
						message.

						Oneway. No TTL must be used for fire and forget
						patterns.

						Request-reply. The TTL must be set.

						Solicit-response. The TTL must be set.

						Notification. For events the TTL must be set.
						For actions the TTL must not be used.
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="RefToMessage" type="xsd:string" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>
						Reference to a MessageId value of an earlier
						message. Its use depends on the MEP.

						One-way. RefToMessageId must not be used.

						Request-reply. The RefToMessageId of the reply
						message contains a copy of the MessageId of the
						request message.

						Solicit-response. The RefToMessageId of the
						response message contains a copy of the
						MessageId of the solicit message.

						Notification. RefToMessageId must not be used.
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="RetryCnt" type="xsd:int" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>
						Integer indicating how many times the message
						was retried from a sender’s perspective.
						(Transport redeliveries are not included).

						No element or a RetryCnt of 0 means the original
						delivery.

						A RetryCnt of n means that the original message
						was sent and n retries were sent as well.
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name="SOAType">
		<xsd:annotation>
			<xsd:documentation>
        		The most common option. In the case of a SOA service.
        	</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="Service">
				<xsd:annotation>
					<xsd:documentation>
						The service structure.
					</xsd:documentation>
				</xsd:annotation>
				<xsd:complexType>
					<xsd:sequence>
						<xsd:element name="Name" type="xsd:string">
							<xsd:annotation>
								<xsd:documentation>Service name.
          </xsd:documentation>
							</xsd:annotation>
						</xsd:element>
						<xsd:element name="Version" type="xsd:string">
							<xsd:annotation>
								<xsd:documentation>Service version.
          </xsd:documentation>
							</xsd:annotation>
						</xsd:element>
					</xsd:sequence>
				</xsd:complexType>
			</xsd:element>
			<xsd:element name="Operation">
				<xsd:annotation>
					<xsd:documentation>
						Operation structure.
					</xsd:documentation>
				</xsd:annotation>
				<xsd:complexType>
					<xsd:sequence>
						<xsd:element name="Name" type="xsd:string">
							<xsd:annotation>
								<xsd:documentation>Operation name.
          </xsd:documentation>
							</xsd:annotation>
						</xsd:element>
						<xsd:element name="Version" type="xsd:string">
							<xsd:annotation>
								<xsd:documentation>Operation version.</xsd:documentation>
							</xsd:annotation>
						</xsd:element>
					</xsd:sequence>
				</xsd:complexType>
			</xsd:element>
			<xsd:element name="Action" type="xsd:string">
				<xsd:annotation>
					<xsd:documentation>
						The action that is performed in the operation.
						This is the standardised IT equivalent of the
						verb part of the operation name, which is
						described from a Business point of view.
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name="EAIType">
		<xsd:annotation>
			<xsd:documentation>
        		For an EAI pattern in case a SOA service cannot be
        		defined.
        	</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="Action" type="xsd:string">
				<xsd:annotation>
					<xsd:documentation>
						The EAI action that is being performed.
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name="AddressingType">
		<xsd:annotation>
			<xsd:documentation>
        		Addressing structure for the message containing its
        		destinations.
        	</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="To" type="xsd:string">
				<xsd:annotation>
					<xsd:documentation>
						Destination of the message. E.g. the queue name
						in case of a JMS transport, or the URL in case
						of a HTTPS transport.
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="ReplyTo" type="xsd:string" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>
						Return address, used for certain MEPs. E.g. in
						case of a Request, in a Request-Reply pattern,
						it contains the destination where the reply is
						expected.
					</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name="TraceHeaderListType">
		<xsd:annotation>
			<xsd:documentation>
				List of 1 or more TraceHeader elements. The TraceHeader
				information in this list is an historical list of all
				previous invoked services and is extended with 1 new
				TraceHeader when a next invocation is initated. e.g. an
				abstracted service invoking multiple other services will
				not use the resulting TraceHeaderList of the service
				responses, only the original message received. This list
				will have the most recent TraceHeader on the bottom.
			</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="TraceHeader" type="TraceHeaderType" maxOccurs="unbounded"/>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name="TraceHeaderType">
		<xsd:sequence>
			<xsd:element name="From" type="FromType" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Copy of the From element.
          </xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="To" type="ToType" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Copy of the To element.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="CollaborationInfo" type="CollaborationInfoType" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Copy of the CollaborationInfo element.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="MessageInfo" type="MessageInfoType" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation>Copy of the MessageInfo element.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:complexType>
</xsd:schema>